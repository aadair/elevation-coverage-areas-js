"use strict";
define([
        // For emitting events
        "dojo/Evented",

        // needed to create a class
        "dojo/_base/declare",
        "dojo/_base/lang",

        // widget class
        "dijit/_WidgetBase",

        // accessibility click
        "dijit/a11yclick",

        // templated widget
        "dijit/_TemplatedMixin",
        "dijit/_WidgetsInTemplateMixin",

        // some UI elements
        "dijit/form/CheckBox",
        "dijit/form/HorizontalSlider",

        // handle events
        "dojo/on",

        // load template
        "dojo/text!coverage-areas-js/templates/coverage-areas.html",

        // localization
        "dojo/i18n!coverage-areas-js/nls/coverage-areas",

        // wait for dom to be ready
        "dojo/domReady!"
    ],
    function(
        Evented,
        declare, lang,
        _WidgetBase, a11yclick, _TemplatedMixin, _WidgetsInTemplateMixin, CheckBox, HorizontalSlider,
        on,
        dijitTemplate,
        i18n) {
        return declare("coverage-areas-js.coverageareas", [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, Evented], {

            templateString: dijitTemplate,

            options: {
                layer: null,
                displayName: null,
                checked: true,
                imgData: null
            },

            constructor: function(options, srcRefNode) {
                // css classes
                this.css = {};

                // language
                this._i18n = i18n;

                // mix in settings and defaults
                var defaults = lang.mixin({}, this.options, options);

                // create the DOM for this widget
                this.domNode = srcRefNode;

                // set properties
                this.set("layer", defaults.layer);
                this.set("displayName", defaults.displayName);
                this.set("checked", defaults.checked);
                this.set("imgData", defaults.imgData);

                // Set initial value
                if (defaults.checked) {
                    defaults.layer.show();
                } else {
                    defaults.layer.hide();
                }

            },

            destroy: function() {
                // call the superclass method of the same name.
                this.inherited(arguments);
            },

            _checkboxValueChanged: function() {
                if (this.checkboxNode.checked) {
                    this.layer.show();
                } else {
                    this.layer.hide();
                }
            },

            _sliderValueChanged: function() {
                this.layer.setOpacity(this.horizontalSliderNode.value);
                this.imgNode.style.opacity = this.horizontalSliderNode.value;
            }

        });
    });
